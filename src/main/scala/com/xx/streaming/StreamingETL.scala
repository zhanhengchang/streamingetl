package com.xx.streaming

import java.util.Properties
import com.xx.util.{KafkaSink, ZookeeperUtil}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.apache.spark.SparkConf
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.{Seconds, StreamingContext}

object StreamingETL {
  val KAFKA_BROKERS = "192.168.100.51:9092,192.168.100.52:9092,192.168.100.53:9092"
  val ZOOKEEPER_HOSTS_PORTS = "192.168.100.51:2181,192.168.100.52:2181,192.168.100.53:2181"
  val NAMESPACE = "consumers"
  val TOPIC = "ctrip"
  val CLEAN_TOPIC = "ctrip_clean"
  val APP_NAME = "StreamingETL"
  val GLOBAL_PATH = s"/${APP_NAME}/offsets"
  val PARTITION = List(0, 1, 2)
  val TIME_INTERVAL = 5

  def main(args: Array[String]): Unit = {
    val ZK_CLIENT = ZookeeperUtil.getZKClient(ZOOKEEPER_HOSTS_PORTS, NAMESPACE)
    ZK_CLIENT.start()
 
    val conf = new SparkConf().setAppName("SparkStreaming任务。。。。。。")
        .set("spark.dynamicAllocation.enabled", "false")
        .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
        // 确保任务在关闭的时候优雅的关闭，即完成一个完整的batch之后再关闭
        .set("spark.streaming.stopGracefullyOnShutdown", "true")
        // 加入反压机制，自动调整每次读取的数据量
        .set("spark.streaming.backpressure.enabled", "true")
        //序列化后的rdd需要暂存在内存中
        .set("spark.kryoserializer.buffer.max", "64m")
        .set("spark.kryoserializer.buffer", "32m")
        //限制每秒每个消费线程读取每个kafka分区最大的数据量【Spark端限速，非Kafka端限速】
        .set("spark.streaming.kafka.maxRatePerPartition","1000")
        //当程序第一次启动，限制第一次批处理应该消费的数据，因为程序冷启动 队列里面有大量积压，防止第一次全部读取，造成系统阻塞
        .set("spark.streaming.backpressure.initialRate", "1000")

    val kafkaParams = Map[String, Object](
      "group.id" -> APP_NAME,
      "bootstrap.servers" -> KAFKA_BROKERS,
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )

    ZookeeperUtil.ensureZKPathExists(GLOBAL_PATH, ZK_CLIENT)//从GLOBAL_PATH开始创建就可以了，只有一级父路径，可以实现创建
    ZookeeperUtil.ensureZKPathExists(s"${GLOBAL_PATH}/${TOPIC}", ZK_CLIENT)
    val streamingContext = new StreamingContext(conf, Seconds(TIME_INTERVAL))
    val fromOffsets = ZookeeperUtil.getFromOffset(GLOBAL_PATH, TOPIC, PARTITION, ZK_CLIENT)
    val kafkaStream: InputDStream[ConsumerRecord[String, String]]= KafkaUtils.createDirectStream(streamingContext, LocationStrategies.PreferConsistent, ConsumerStrategies.Subscribe(Array(TOPIC), kafkaParams, fromOffsets))
    println("中断后Streaming成功！")

    //应当避免对于每条记录都创建一个 KafkaProducer，然后利用 KafkaProducer 进行输出操作，因为这样效率低下，也占用资源。
    //所以希望能共用KafkaProducer。但由于KafkaProducer 是不可序列化的（意味着一个KafkaProducer不能跨机器使用），所以不能将 KafkaProducer 实例放在 foreachRDD 外边创建。
    //（类似数据库连接对象  connection  不能序列化，所以也不能放在外面）
    //因此选择创建Kafka包装器，使用广播变量为每个执行程序提供自己的包装 KafkaProducer 实例。
    //（为什么放在foreachRDD外面不会每个Worker上都有KafkaProducer这个实例呢？？？因为是Driver或ApplicationMaster将Task发送到Worker，每个Worker上并不会都有KafkaProducer实例）
    //广播KafkaSink
    val kafkaProducer: Broadcast[KafkaSink[String, String]] = {
      val kafkaProducerConfig = {
        val p = new Properties()
        p.setProperty("bootstrap.servers", KAFKA_BROKERS)
        //这两个序列化的类不要引错了，不然会报异常：“org.apache.kafka.common.KafkaException: Failed to construct kafka producer”
        p.setProperty("key.serializer", classOf[StringSerializer].getName)
        p.setProperty("value.serializer", classOf[StringSerializer].getName)
        p
      }
      println("kafka producer 初始化完成！")
      streamingContext.sparkContext.broadcast(KafkaSink[String, String](kafkaProducerConfig))
    }

    kafkaStream.foreachRDD( rdd => {
      if (rdd.isEmpty()){
        println("RDD是空的--------------------------------------------------------")
      } else {
        rdd.collect()
          .map(KV => (KV.key(), KV.value()))
          .filter(KV => (KV._2 != null && KV._2.split("\t").length == 12))
          .map(KV => KV._2)   //截至这一步得到了干净数据
          .foreach(value => {
            println(value)
            kafkaProducer.value.send(CLEAN_TOPIC, value)
          })//将干净数据写回到Kafka的另一个Topic
        // 存储新的offset
        ZookeeperUtil.storeOffsets(rdd.asInstanceOf[HasOffsetRanges].offsetRanges, GLOBAL_PATH, ZK_CLIENT)
      }
    })

    kafkaStream.start()
    streamingContext.start()
    streamingContext.awaitTermination()
  }
}